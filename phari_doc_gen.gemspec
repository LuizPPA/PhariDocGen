Gem::Specification.new do |s|
  s.required_ruby_version = '>= 2.0'
  s.add_runtime_dependency 'activesupport', '~> 5.1', '>= 5.1.4'
  s.name        = 'phari_doc_gen'
  s.version     = '3.0.1'
  s.date        = '2018-01-03'
  s.summary     = "Main classes: PhariDocGen && FileHandler"
  s.description = " Hi there, friend! PhariDoc Generator is a simple
                    and functional documentation generator for Ruby web aps.
                    PhariDoc Generator will provide you an easy way
                    to generate documentation for your projects. The result
                    will be a project page with a description that can be written using a simplified
                    markdown syntax. Also, each ruby
                    Class will have it's own page especifying it's relatios helper methods and controller
                    routes including their inputs, outputs and descriptions. The gem includes
                    an executable file and the classes that can be used individually,
                    providing the possibility to use the generator for any project
                    structure."
  s.authors     = ["Phari Solutions"]
  s.email       = 'luiz@phari.solutions'
  s.files       = [ "lib/phari_doc_gen.rb",
                    "lib/phari_doc_gen/FileHandler.rb",
                    "lib/phari_doc_gen/MethodParam.rb",
                    "lib/phari_doc_gen/Metodo.rb",
                    "lib/phari_doc_gen/Modelo.rb",
                    "lib/phari_doc_gen/Rota.rb"]
  s.homepage    = 'https://gitlab.com/LuizPPA/PhariDocGen'
  s.license     = 'Beerware'
  s.executables = ['phari_doc_gen']
end
